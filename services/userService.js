const fs = require("fs");
var sha512 = require("js-sha512");

// const userData = {
//   id: 1,
//   email: "avkash@innovify.com",
//   name: "Avkash Chevli",
//   password: sha512("123456")
// };
class UserService {
  //   constructor() {
  //     const usr = JSON.parse(fs.readFileSync("./json/user.json"));
  //     usr.push(userData);
  //     const temp = fs.writeFileSync("./json/user.json", JSON.stringify(usr));
  //     //const json = JSON.parse(temp);
  //     console.log(usr);
  //   }
  /**
   * This function use to add user
   * @param {Object}  user  user required details to add user
   * @returns {Object} success or false object
   */
  static addUser(user) {
    let success = true;
    let msg = "success";
    const fetchData = JSON.parse(fs.readFileSync("./json/user.json"));
    user = { id: fetchData.length + 1, ...user };
    fetchData.push(user);
    console.log(fetchData);
    fs.writeFile("./json/user.json", JSON.stringify(fetchData), (err, data) => {
      if (err) {
        success = false;
        msg = err;
      }
    });

    return {
      status: {
        success: success,
        message: msg
      }
    };
  }

  /**
   * This function use to remove user
   * @param {Integer}  userId  user primary id to remove user from your users list
   * @returns {Object} success or false object
   */
  static removeUser(userId) {
    let success = true;
    let msg = "success";
    const fetchData = JSON.parse(fs.readFileSync("./json/user.json"));
    const list = fetchData.filter(data => data.id != userId);
    fs.writeFile("./json/user.json", JSON.stringify(list), (err, data) => {
      if (err) {
        success = false;
        msg = err;
      }
    });

    return {
      status: {
        success: success,
        message: msg
      }
    };
  }

  /**
   * This function use to view user details
   * @param {Integer}  userId  user primary id to view user details
   * @returns {Object} user details object
   */
  static viewUser(userId) {
    const fetchData = JSON.parse(fs.readFileSync("./json/user.json"));
    const list = fetchData
      .filter(data => data.id == userId)
      .map(data => ({
        id: data.id,
        name: data.name,
        email: data.email
      }));

    return {
      status: {
        success: true,
        message: "success"
      },
      data: list
    };
    // return {
    //   status: {
    //     success: true,
    //     message: "success"
    //   },
    //   data: {
    //     id: 1,
    //     name: "Alice",
    //     email: "alice@mailinator.com"
    //   }
    // };
  }

  /**
   * This function use to view all users list
   * @param {Integer}  id  Logged in user primary id to get all users
   * @returns {Object} all users object
   */
  static listUsers(id) {
    const fetchData = JSON.parse(fs.readFileSync("./json/user.json"));
    const list = fetchData.map(data => ({
      id: data.id,
      name: data.name,
      email: data.email
    }));
    return {
      status: {
        success: true,
        message: "success"
      },
      data: list
    };
    // return {
    //   status: {
    //     success: true,
    //     message: "success"
    //   },
    //   data: [
    //     {
    //       id: 1,
    //       name: "Alice",
    //       email: "alice@mailinator.com"
    //     },
    //     {
    //       id: 2,
    //       name: "Roy",
    //       email: "roy@mailinator.com"
    //     }
    //   ]
    // };
  }
}

module.exports = UserService;
