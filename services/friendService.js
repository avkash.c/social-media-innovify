const fs = require('fs');
var standard_input = process.stdin;
standard_input.setEncoding('utf-8');
class FriendService {
    /**
     * This function use to add friend
     * @param {Object}  friend  friend required details to add friend 
     * @returns {Object} success or false object
     */
    static async addFriend(friend){
        var fileDetails;
        var friendsDetails;
        try {
            fileDetails = fs.readFileSync('./json/friends.json');  
            friendsDetails = JSON.parse(fileDetails);  
        } catch (error) {
            friendsDetails = [];
        }
        
        console.log(fileDetails.toString);
        
        
        const userDetails = JSON.parse(fs.readFileSync('./json/user.json'));
        const currentLoginUserDetails = JSON.parse(fs.readFileSync('./json/currentLoginUser.json'));
        
        const userDetailsOfFriend = userDetails.filter(data =>  data.id == friend);
        const requiredDetailsofFriend = {id: userDetailsOfFriend[0].id, name: userDetailsOfFriend[0].name, email: userDetailsOfFriend[0].email};
        
        if(friendsDetails.length < 1){
            friendsDetails.push({id: 1, name: currentLoginUserDetails[0].name, email: currentLoginUserDetails[0].email, friends: [requiredDetailsofFriend] })
            console.log(friendsDetails);
            fs.writeFileSync('./json/friends.json',JSON.stringify(friendsDetails,null,2));
            return "Friend added successfully";
        } else {
            const existingFriendDetail = friendsDetails.filter(data => data.email === currentLoginUserDetails[0].email)
            console.log("existing",existingFriendDetail);
            if(existingFriendDetail != null){
                // const currentFriendDetails = existingFriendDetail.filter(data => data.email == currentLoginUserDetails[0].email);
                existingFriendDetail[0].friends.push({...requiredDetailsofFriend});
                console.log(friendsDetails);
                fs.writeFileSync('./json/friends.json',JSON.stringify(friendsDetails,null,2));
                
            } else {
                friendsDetails.push({id: friendsDetails.length+1, name: currentLoginUserDetails[0].name, email: currentLoginUserDetails[0].email, friends: [requiredDetailsofFriend] })
                console.log(friendsDetails);
                fs.writeFileSync('./json/friends.json',JSON.stringify(friendsDetails,null,2));
                
            }  
            return "Friend added successfully";    
        }
        
    }

    /**
     * This function use to remove friend
     * @param {Integer}  friendId  friend primary id to remove friend from your friends list
     * @returns {Object} success or false object
     */
    static async removeFriend(friendId){
        let friendsDetails = JSON.parse(fs.readFileSync('./json/friends.json'));
        const currentLoginUserDetails = JSON.parse(fs.readFileSync('./json/currentLoginUser.json'));

        const userDetail = friendsDetails.filter(data => data.email === currentLoginUserDetails[0].email);

        const myFriends = userDetail[0].friends.filter(data => data.id != friendId);
        userDetail[0].friends=[];
        userDetail[0].friends.push(...myFriends);
        fs.writeFileSync('./json/friends.json',JSON.stringify(friendsDetails,null,2));
        return "Successfully deleted friend record";
    }

    /**
     * This function use to view friend details
     * @param {Integer}  friendId  friend primary id to view friend details
     * @returns {Object} friend details object
     */
    static async viewFriend(friendId) {  
        
        let friendDetails = await JSON.parse(fs.readFileSync('./json/friends.json').toString());
        let userDetails = await JSON.parse(fs.readFileSync('./json/currentLoginUser.json').toString());
        let userId = userDetails[0].id;
        let friendDetail = await friendDetails.filter(data => data.id==userId);
        
        let myFriends = friendDetail[0].friends;
        let result = myFriends.filter(data => data.id == friendId);
    
        if(!result || result.length < 1){
            return new Error("Unable to find friend with given id");
        } else {
            return result;
        }
    }

    /**
     * This function use to view all friends list
     * @param {Integer}  id  Logged in user primary id to get all friends
     * @returns {Object} all friends object
     */
    static async viewFriends(id){
     
        const friendDetails = await JSON.parse(fs.readFileSync('./json/friends.json').toString());
        const userDetails = await JSON.parse(fs.readFileSync('./json/currentLoginUser.json').toString());
        const userId = userDetails[0].id;
        if(id == userId){
            const friendDetail = await friendDetails.filter(data => data.id == userId); 
            if(!friendDetail || friendDetail.length < 1){
                return new Error("Unable to find friend");
            } else {
                const myFriends = friendDetail[0].friends;
                return myFriends;
            }
        } else {
            return new Error("Unable to find user with given id");
        }       
    }

    }
    
    module.exports = FriendService;