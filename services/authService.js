const fs = require("fs");

class AuthService {
  /**
   * This function use to login/signin user into system.
   * @param {string}  email  email required details
   * @param {string}  password  password required details
   * @returns {Object} success or false object
   */
  static signin(email, password) {
    let success = true;
    let msg = "success logedin";
    const fetchData = JSON.parse(fs.readFileSync("./json/user.json"));
    const login = fetchData.filter(
      data => data.email == email && data.password == password
    );

    if (login.length == 1) {
      fs.writeFile(
        "./json/currentLoginUser.json",
        JSON.stringify(login),
        (err, data) => {
          if (err) {
            success = false;
            msg = err;
          }
        }
      );
    } else {
      success = false;
      msg = "Invalid Credential";
    }

    return {
      status: {
        success: success,
        message: msg
      }
    };
  }

  /**
   * This function use to logout from system
   * @param {string}  email  email to logout user from system
   * @returns {Object} success or false object
   */
  static logout(email) {
    return {
      status: {
        success: true,
        message: "success"
      }
    };
  }
}

module.exports = AuthService;
