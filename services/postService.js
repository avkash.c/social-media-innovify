const fs=require('fs')

class PostService {
    static addPost(post) {
        const currLoggedUser = JSON.parse(fs.readFileSync('./json/currentLoginUser.json'));
        const readObj=JSON.parse(fs.readFileSync('./json/posts.json'))
        readObj.push(
            {
                id:readObj[readObj.length-1].id+1,
                text:post.replace(/\n/g,''),
                postBy:currLoggedUser[0].name,
                likes:0,
            })
        fs.writeFileSync('./json/posts.json',JSON.stringify(readObj,null,2))
        return readObj
    }

    static getposts() {
        const friendDetails = JSON.parse(fs.readFileSync('./json/friends.json'));
        const currLoggedUser = JSON.parse(fs.readFileSync('./json/currentLoginUser.json'));
        const userId =currLoggedUser[0].id;

        const friendDetail = friendDetails.filter(data => data.id==userId);
        if(!friendDetail || friendDetail.length < 1){
            return new Error("Unable to find friend");
            } else {
                const myFriends = friendDetail[0].friends;
                for (const eachFrnd of myFriends) {
                    this.getPost(eachFrnd.id)
                }
            }
    }

    static getPost(id) {
        const readObj=JSON.parse(fs.readFileSync('./json/posts.json'))
        const viewObj=readObj.filter(data => data.id==id)
        console.log(viewObj)
        return viewObj 
    }

    static deletePost(id) {
        const readObj=JSON.parse(fs.readFileSync('./json/posts.json'))
        const newObj=readObj.filter(data => data.id!=id)
        fs.writeFileSync('./json/posts.json',JSON.stringify(newObj,null,2))
        console.log(newObj)
    }
}

module.exports = PostService;
