var standard_input = process.stdin;
standard_input.setEncoding("utf-8");

class Utility {
  static getInput(message) {
    return new Promise((resolve, reject) => {
      console.log(message);
      standard_input.removeAllListeners();
      standard_input.on("data", resolve);
    });
  }
}
module.exports = Utility;
