const UserService = require("./services/userService.js");
const PostService = require("./services/postService");
const FriendService = require("./services/friendService.js");
const Utility = require("./services/util.js");
const Util = require("./services/util");
const AuthService = require("./services/authService.js");
const fs = require("fs");

var standard_input = process.stdin;
standard_input.setEncoding("utf-8");
var sha512 = require("js-sha512");
// const userObj = new UserService();
//const utilObj = new Utility();

function main() {
  const login = JSON.parse(
    fs.readFileSync("./json/currentLoginUser.json").toString()
  );
  if (login.length != 1) {
    displayMenu();
  } else {
    displayLogedInMenu();
  }
}

main();

async function displayMenu() {
  console.log("Please select an option from below to continue.");
  console.log("1. Login");
  console.log("2. Add User (Sign up)");
  console.log("3. Remove User");
  console.log("4. List Users");
  console.log("5. View User");
  console.log("Press exit to terminate");

  // User input exit.
  const data = await Utility.getInput("");
  var userid;
  switch (parseInt(data)) {
    case 1:
      const loginid = await Utility.getInput("\nEnter your email: ");
      let loginpass = await Utility.getInput("\nEnter password: ");
      loginpass = sha512(loginpass);
      const auth = await AuthService.signin(
        loginid.replace(/\n/g, ""),
        loginpass.replace(/\n/g, "")
      );
      console.log(auth.status);
      break;
    case 2:
      const email = await Utility.getInput("\nEnter email:");
      const name = await Utility.getInput("\nEnter name:");
      let pass = await Utility.getInput("\nEnter password:");
      pass = sha512(pass);
      var obj = {
        email: email.replace(/\n/g, ""),
        name: name.replace(/\n/g, ""),
        password: pass.replace(/\n/g, "")
      };
      console.log(UserService.addUser(obj));
      break;
    case 3:
      userid = await Utility.getInput("Enter User Id to remove user");
      console.log();
      console.log(UserService.removeUser(userid));
      console.log();
      break;
    case 4:
      console.log();
      console.log(UserService.listUsers());
      console.log();
      break;
    case 5:
      userid = await Utility.getInput("Enter User Id to see details");
      console.log();
      console.log(UserService.viewUser(userid));
      console.log();
      break;
    default:
  }
  if (data === "exit\n") {
    // Program exit.
    console.log("User input complete, program exit.");
    process.exit();
  } else {
    // Print user input in console.
    //   console.log("User Input Data : " + data);
    // const login = JSON.parse(fs.readFileSync("./json/currentLoginUser.json"));
    displayMenu();
  }
}

function displayLogedInMenu() {
  console.log("Please select an option from below to continue.");
  console.log("1. View all users");
  console.log("2. View posts made by your friends");
  console.log("3. View friends");
  console.log("4. View friend");
  console.log("5. View post");
  console.log("6. Add post");
  console.log("7. Add friend");
  console.log("8. Delete post");
  console.log("9. Delete friend");
  console.log("10. Logout");
  console.log("Press exit to terminate");

  standard_input.on("data", async function(data) {
    switch (parseInt(data)) {
      case 1:
      case 2: 
        try {
          await PostService.getposts();
          
        } catch (err) {
          console.log(err);
      
        }
        break;
      case 3: 
        try {
          const id = await Util.getInput("Please enter your Id");
          const result = await FriendService.viewFriends(id);
          console.log(result);
          
        } catch (err) {
          console.log(err.message);
          
        }
      break;
      case 4: 
        try {
          const id = await Util.getInput("Please enter your friend's id");
          console.log(await FriendService.viewFriend(id));
          
        } catch (err) {
          console.log(err.message);
          
        }
        break;
      case 5: 
        try {
          const id = await Util.getInput(
            "Please enter id of the post u want to view!!!"
          );
          await PostService.getPost(id);
          
        } catch (err) {
          console.log(err.message);
          
        }
        break;
      case 6: 
        try {
          const post = await Util.getInput("Please enter post!!!");
          await PostService.addPost(post);
          
        } catch (err) {
          console.log(err.message5);
          
        }
        break;
      case 7: 
        try {
          const id = await Util.getInput("Please enter friend Id to add");
          await FriendService.addFriend(id);
          
        } catch (err) {
          console.log(err);
          
        }
      break;
      case 8: 
        try {
          const id = await Util.getInput(
            "Please enter id of the post u want to delete!!!"
          );
          await PostService.deletePost(id);
      
        } catch (err) {
          console.log(err.message);
      
        }
        break;
      case 9: 
        try {
          const id = await Util.getInput(
            "Please enter your friend's id to remove"
          );
          await FriendService.removeFriend(id);
          
        } catch (err) {
          console.log(err.message);
          
        }
        break;
      case 10:
      default:
        console.log("default");
    }
    if (data === "exit\n") {
      // Program exit.
      console.log("User input complete, program exit.");
      process.exit();
    } else {
      // Print user input in console.
      console.log("User Input Data : " + data);
      displayLogedInMenu();
    }
  });
}
//displayLogedInMenu();
